# IDE / Text Editor

### [Brackets](http://brackets.io) Utviklet for web design.
### [Sublime](https://www.sublimetext.com) Lettvekt og minemalistisk med plug-in muligheter for behov.
### [VS Code](https://code.visualstudio.com) Lettvekt Visual Studio designet for mange språk.
### [Atom](https://atom.io) Ligger et sted i mellom Sublime og VS Code.


# Git

### Windows
[Git for Windows](https://gitforwindows.org)

Fullfør installsjon og åpne kommandolinjen (`⊞ Win`+`R`, kjør `cmd.exe`).

Fortsett videre på lik linje med Linux / macOS.

### Linux / macOS

Klone repo til deres maskin:
```
git clone https://bitbucket.org/aleksaaz/bprog-30oct-web.git
```

Mappen `bprog-30oct-web` skal nå være lastet ned med alt innhold.